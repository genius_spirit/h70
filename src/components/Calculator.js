import React, {Fragment} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {connect} from "react-redux";
import KeyboardButton from "./KeyboardButton";
import Screen from "./Screen";
import * as actionTypes from "../store/actionTypes";

const KEYBOARD = ['7', '8', '9', '+', '4', '5', '6', '-', '1', '2', '3', '*', '0', '(', ')', '/', '.'];

class Calculator extends React.Component {
  render() {
    return (
      <Fragment>
        <View style={styles.screen}>
          <Screen func={this.props.total} calc={this.props.calc}/>
        </View>
        <View style={styles.keyboard}>
          {KEYBOARD.map((item, index) => (
            <KeyboardButton key={index} buttons={item} buttonPressed={() => this.props.sendItem(item)}/>
          ))}
          <TouchableOpacity onPress={this.props.removeLastValue}><View style={styles.button}><Text style={styles.buttonText}>{'<'}</Text></View></TouchableOpacity>
          <TouchableOpacity onPress={this.props.getZero}><View style={styles.button}><Text style={styles.buttonText}>{'C'}</Text></View></TouchableOpacity>
          <TouchableOpacity onPress={this.props.calculate}><View style={styles.button}><Text style={styles.buttonText}>{'='}</Text></View></TouchableOpacity>
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  keyboard: {
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between'
  },
  button: {
    width: 102.7,
    height: 101.5,
    backgroundColor: '#333'
  },
  buttonText: {
    color: '#eee',
    fontSize: 40,
    textAlign: 'center',
    lineHeight: 102.5
  }
});

const mapStateToProps = state => {
  return {
    total: state.total,
    calc: state.calc
  }
};

const mapDispatchToProps = dispatch => {
  return {
    sendItem: (item) => dispatch({type: actionTypes.SEND_ITEM, amount: item}),
    removeLastValue: () => dispatch({type: actionTypes.REMOVE}),
    calculate: () => dispatch({type: actionTypes.CALCULATE}),
    getZero: () => dispatch({type: actionTypes.GET_ZERO})
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Calculator);