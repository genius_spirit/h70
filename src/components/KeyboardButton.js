import React from 'react';
import {Text, TouchableOpacity, View, StyleSheet} from "react-native";

const KeyboardButton = (props) => {
  return(
    <TouchableOpacity onPress={props.buttonPressed}>
      <View style={styles.keyboardButtons} >
        <Text style={styles.buttonsText}>{props.buttons}</Text>
      </View>
    </TouchableOpacity>
  )
};

const styles = StyleSheet.create({
  keyboardButtons: {
    width: 102.7,
    height: 101.5,
    backgroundColor: '#333'
  },
  buttonsText: {
    color: '#eee',
    fontSize: 45,
    textAlign: 'center',
    lineHeight: 102.5
  }
});

export default KeyboardButton;