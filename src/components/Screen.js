import React, {Fragment} from 'react';
import {Text, StyleSheet} from "react-native";

const Screen = (props) => {
  return (
    <Fragment>
      <Text style={styles.screenUpperLine}>{props.func}</Text>
      <Text style={styles.screenLowerLine}>{props.calc}</Text>
    </Fragment>
  )
};
const styles = StyleSheet.create({
  screenUpperLine: {
    fontSize: 30,
    padding: 10,
    color: '#333',
    textAlign: 'left'
  },
  screenLowerLine: {
    textAlign: 'right',
    fontSize: 50,
    padding: 10
  },
});

export default Screen;

