import React from 'react';
import {Provider} from "react-redux";
import {createStore} from "redux";
import {StyleSheet, View} from "react-native";
import calcReducer from './src/store/reducers/calculator';
import Calculator from "./src/components/Calculator";

const store = createStore(calcReducer);

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <Calculator />
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop: 30
  }
});
